%global _empty_manifest_terminate_build 0
Name:		python-bandit
Version:	1.7.10
Release:	1
Summary:	Security oriented static analyser for python code.
License:	Apache-2.0
URL:		https://github.com/PyCQA/bandit
Source0:	https://files.pythonhosted.org/packages/38/26/bdd962d6ee781f6229c3fb83483cf9e09d87959150a9000789806d750f3c/bandit-1.7.10.tar.gz
BuildArch:	noarch


%description
A security linter from PyCQA

%package -n python3-bandit
Summary:	Security oriented static analyser for python code.
Provides:	python-bandit
BuildRequires:	python3-devel
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:	python3-setuptools
%description -n python3-bandit
A security linter from PyCQA

%package help
Summary:	Development documents and examples for bandit
Provides:	python3-bandit-doc
%description help
A security linter from PyCQA

%prep
%autosetup -n bandit-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-bandit -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Oct 08 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 1.7.10-1
- Upgrade package to version 1.7.10
  - Bump docker/build-push-action from 5.4.0 to 6.0.0 by @dependabot in #1147
  - Suggested small refactors in assignments by @ericwb in #1150
  - Performance improvement in blacklist function by @ericwb in #1148
  - Add test for usage of FTP_TLS by @ericwb in #1149
  - New check: B113: TrojanSource - Bidirectional control characters by @Lucas-C in #757

* Mon Jul 29 2024 Hann <hannannan@kylinos.cn> - 1.7.9-1
- Upgrade package to version 1.7.9
- Fix crash on pyproject.toml without bandit config
- Fix up issues found running Bandit on itself

* Tue Apr 11 2023 wangkai <13474090681@163.com> - 1.7.5-1
- Upgrade package to version 1.7.5

* Sat Oct 08 2022 guozhengxin <guozhengxin@kylinos.cn> - 1.7.4-1
- Upgrade package to version 1.7.4

* Sat Jul 31 2021 huangtianhua <huangtianhua@huawei.com> - 1.7.0-2
- Adds pbr/pip as buildrequires
* Wed Feb 03 2021 liusheng <liusheng2048@gmail.com>
- Initial package
